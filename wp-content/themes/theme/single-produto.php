<?php
    //Template Post Type: produto
?>

<?php
    $post_type = 'produto';
?>

<?php
    echo get_header();
?>

<main>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/CSS/paginas-single.css">
    <?php
        echo "<div class='produto-single'>";
            echo "<h1>";
                the_title();
            echo "</h1>";

            echo "<div class='inner-content'>";
                while (have_posts()) {
                    the_post();
                    the_content();
                }
            echo "</div>";
        echo "</div>";
    ?>
    <div class="related-items">
        <?php
            show_posts($post_type, 3, $paged, '', 'rand');
        ?>
    </div>
</main>


<?php 
    echo get_footer();
?>