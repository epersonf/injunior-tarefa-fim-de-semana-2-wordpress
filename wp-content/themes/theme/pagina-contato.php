<?php
    //Template Name: Pagina Contato
?>

<?php
    echo get_header();
?>

<main>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/CSS/pagina-contato.css">
    <div id="form">
        <?php
            while (have_posts() ):
                the_post();
                the_content();
            endwhile; ?>
    </div>
</main>
<?php 
    echo get_footer();
?>