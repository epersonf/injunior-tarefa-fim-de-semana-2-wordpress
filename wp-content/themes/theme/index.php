<?php
    echo get_header();
?>

<main>
	<section id="section-1">
        
        <img src="<?php the_field('imagem_secao_1') ?>" alt="imagem chamativa">

        <p><?php the_field('texto_secao_chamativa') ?></p>

    </section>
    <section id="section-2">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin posuere efficitur sodales. Sed in quam ante. Donec feugiat cursus nunc vehicula suscipit. Praesent mollis aliquam neque at commodo. Nam tempus libero sed volutpat convallis. Phasellus consectetur eros rhoncus est convallis gravida. Fusce sodales lectus ac nisi fermentum, lobortis dapibus velit molestie. In id sem tortor. Pellentesque ultricies nisl non enim tincidunt gravida ut a ligula. Vivamus feugiat, nulla eget semper tempus, enim eros aliquet neque, dictum rutrum diam justo ut dui. Proin dictum eget libero in feugiat. Aliquam congue dui a nulla mollis, ac luctus tellus mattis. Mauris aliquet, massa id sagittis placerat, tellus dolor maximus sapien, vel eleifend odio mi ut augue. Fusce egestas tellus ac neque porttitor pulvinar.</p>
        <a href="">página sobre</a>
    </section>
    <section id="section-3">

        <?php show_posts('produto', 4, 1, '', 'rand'); ?>

        <a href="">Ver mais produtos.</a>
    </section>
    <section id="section-4">

        <?php show_posts('noticia', 4, 1, '', 'date'); ?>

        <a href="">Ver mais noticias.</a>
    </section>
</main>


<?php 
    echo get_footer();
?>