<?php
    //Template Name: Pagina Produtos
?>

<?php
    $post_type = 'produto';
    $posts_per_page = 4;
    $paged = get_query_var('paged', 1);
?>

<?php
    echo get_header();
?>

<main>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/CSS/pagina-de-mostruario.css">

    <h1>Nossos produtos:</h1>

    <div class="show-container products">

        <!-- Exibir posts -->
        <?php $loop = show_posts($post_type, $posts_per_page, $paged, 'Nao temos produtos, me desculpe.'); ?>
    </div>
    
    <!-- Paginacao -->
    <div id="pagination">
        <?php
            generate_pagination($loop);
            wp_reset_postdata();
        ?>
    </div>
</main>


<?php 
    echo get_footer();
?>