<?php
    //Template Name: Pagina Noticias
?>

<?php
    $post_type = 'noticia';
    $posts_per_page = 2;
    $paged = get_query_var('page', 1);
?>


<?php
    echo get_header();
?>

<main>
    <?php get_search_form(); ?>

    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/CSS/pagina-de-mostruario.css">
    <div class="show-container news">
        <!-- Exibir posts -->
        <?php $loop = show_posts($post_type, $posts_per_page, $paged, "Nao temos nada a ser mostrado, me desculpe.", 'date'); ?>
    </div>
    
    <!-- Paginacao -->
    <div id="pagination">
        <?php
            generate_pagination($loop);
            wp_reset_postdata();
        ?>
    </div>
</main>


<?php 
    echo get_footer();
?>