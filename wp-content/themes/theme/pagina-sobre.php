<?php
    //Template Name: Pagina Sobre
?>

<?php
    echo get_header();
?>

<main>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/CSS/pagina-sobre.css">
    <div id="sobre">
        <img src="<?php the_field('imagem_pagina_sobre') ?>" alt="Imagem de café com coração">
        <p><?php the_field('texto_pagina_sobre') ?></p>
    </div>

    <div id="social">
        <?php show_posts("socialmedia", 999999 , 1, "") ?>
    </div>
</main>


<?php 
    echo get_footer();
?>