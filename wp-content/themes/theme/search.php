<?php
    // Template Name: Search Page
?>

<?php get_header(); ?>

<h2>Resultados da pesquisa:</h2>
<br>
<?php get_search_form(); ?>

<?php
    $search_name = get_query_var('s', "");

    console_log($search_name);

    $loop = new WP_Query(
        array(
            'post_type' => 'noticia',
            's' => $search_name
        )
    );

    if ($loop -> have_posts()) {
        while($loop -> have_posts()) {
            $loop -> the_post();

            echo "<h3 class='search-result'>";
                echo "<a href='", the_permalink(), "'>";
                    the_title();
                echo "</a>";
            echo "</h3>";

        }
    }
    wp_reset_postdata();
    echo paginate_links();
?>

<?php get_footer(); ?>