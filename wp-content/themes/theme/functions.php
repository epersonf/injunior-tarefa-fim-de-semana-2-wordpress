<?php
    
    /* Registrando post do tipo produto */
    add_action( 'init',  function() {
        lc_register_custom_post_type("produto", "Produtos", "Produto", "Produtos do site.");
        lc_register_custom_post_type("noticia", "Noticias", "Noticia", "Noticias do site.");
        lc_register_custom_post_type("socialmedia", "SocialMedias", "SocialMedia", "Midias sociais.");
    });

    // Funcao de registrar post type
    function lc_register_custom_post_type($name_id, $name, $singular_name, $description) {

        // Seta configuracoes dentro do array
        $labels = array(
            'name' => _x( $name, 'post type general name' ),
            'singular_name' => _x( $singular_name, 'post type singular name' )
        );

        $args = array(
            'labels' => $labels,
            'description' => $description,
            'public' => true
        );

        // Registra o tipo de post
        register_post_type( $name_id, $args );
    }


    
    // inserindo os arquivos de estilo
    function site_estilos() {
        wp_enqueue_style( "reset-sheet", get_stylesheet_directory_uri() . "/css/reset.css");
        wp_enqueue_style( "style-sheet", get_stylesheet_directory_uri() . "/style.css");
    }
    add_action('wp_enqueue_scripts', 'site_estilos');

    // menu de navegaçao
    add_theme_support( "menus" );

    function register_my_menu() {
        register_nav_menu( 'navegacao', __('Navegação') );
    }

    add_action( 'init', 'register_my_menu' );

	
	// Funcao para log em JS
    function console_log( $data ){
        echo '<script>';
        echo 'console.log('. json_encode( $data ) .')';
        echo '</script>';
    }
	
    //Essa funcao eh usada em paginas que mostram postagens para mostrar um post de um tipo especifico
    //Ela retorna o WP_Query
    function show_posts($post_type, $posts_per_page, $paged, $error_msg, $order='name') {

        //array de informacao pagina
        $loop = new WP_Query(
            array(
                'post_type' => $post_type,
                'posts_per_page' => $posts_per_page,
                'paged' => $paged,
                'post_status' => 'publish',
                'suppress_filters' => true,
                'orderby' => $order,
                'order' => 'ASC'
            )
        );


        if ($loop -> have_posts()) {
            while ( $loop -> have_posts() ) {
                $loop->the_post();
                switch($post_type) {
                    case 'socialmedia':
                        create_social_media_html();
                        break;
                    default:
                        create_post_html();
                        break;
                }
            }
        } else {
            echo $error_msg;
        }

        return $loop;
    }

    //Essa funcao gera um codigo html de botoes de social media
    function create_social_media_html() {
        echo "<a href=", the_title(), ">";
            echo '<div class="social-media">';
                    the_content();
            echo '</div>';
        echo "</a>";
    }

    //Essa funcao gera um codigo html de botoes de post padrao
    function create_post_html() {
        echo "<a href=", the_permalink(), " class='product-button'>";
            echo "<div>";
                echo "<h2>", the_title(), "</h2>";
                echo "<p>", the_content(), "</p>";
            echo "</div>";
        echo "</a>";
    }

    //Essa funcao gera a paginacao de um dado WP_Query
    function generate_pagination($loop) {
        $big = 99999999;
        echo paginate_links(
            array(
                'base' => str_replace($big, '%#%', get_pagenum_link($big)),
                'format' => '?paged=%#%',
                'current' => max(1, get_query_var('paged')),
                'prev_text' => __('Anterior'),
                'next_text' => __('Próximo'),
                'total' => $loop->max_num_pages
            )
        );
    }
?>