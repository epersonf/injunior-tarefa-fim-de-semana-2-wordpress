<?php
    //Template Name: Home Page
?>

<?php
    echo get_header();
?>

<style>
    #section-1 {
        background-image: url(<?php the_field('imagem_secao_1') ?>);
        background-attachment: fixed;
    }
</style>

<main>

	<section id="section-1">

        <h2><?php the_field('texto_secao_chamativa') ?></h2>

    </section>

    <section id="section-2">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin posuere efficitur sodales. Sed in quam ante. Donec feugiat cursus nunc vehicula suscipit. Praesent mollis aliquam neque at commodo. Nam tempus libero sed volutpat convallis. Phasellus consectetur eros rhoncus est convallis gravida. Fusce sodales lectus ac nisi fermentum, lobortis dapibus velit molestie. In id sem tortor. Pellentesque ultricies nisl non enim tincidunt gravida ut a ligula. Vivamus feugiat, nulla eget semper tempus, enim eros aliquet neque, dictum rutrum diam justo ut dui. Proin dictum eget libero in feugiat. Aliquam congue dui a nulla mollis, ac luctus tellus mattis. Mauris aliquet, massa id sagittis placerat, tellus dolor maximus sapien, vel eleifend odio mi ut augue. Fusce egestas tellus ac neque porttitor pulvinar.</p>
        <form action="<?php echo get_site_url(); ?>/pagina-sobre">
            <input type="submit" value="Saiba mais"/>
        </form>
    </section>

    <section id="section-3">
        <div id="div-produtos">

            <?php show_posts('produto', 4, 1, '', 'rand'); ?>

        </div>
        <form action="<?php echo get_site_url(); ?>/produtos">
            <input type="submit" value="Ver mais produtos"/>
        </form>
    </section>

    <section id="section-4">
        <h3>Ultimas noticias...</h3>

        <div id="div-noticias">
            <?php show_posts('noticia', 4, 1, '', 'date'); ?>
        </div>

        <form action="<?php echo get_site_url(); ?>/noticias">
            <input type="submit" value="Ver mais"/>
        </form>
    </section>
</main>


<?php 
    echo get_footer();
?>